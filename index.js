const express = require("express");
const db = require("./database/connection");

const app = express();
app.use(express.json());
 
app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get("/api/cats", function (req, res) {
  db.query(
    "SELECT * FROM cats",
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

app.post("/api/cats", function (req, res){
  const {name, image} = req.body;
  db.query(
    `INSERT INTO cats (name, image) VALUES ("${name}", "${image}")`,
    function (error, results, fields){
      if (error) throw error;
    }
  );
  db.query(
    "SELECT * FROM cats",
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

app.delete("/api/cats/:id", function (req, res){
  const {id} = req.params;
  db.query(
    `DELETE FROM cats WHERE id=${id}`,
    function (error, results, fields){
      if (error) throw error;
    }
  );
  db.query(
    "SELECT * FROM cats",
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

app.put("/api/cats/:id", function (req, res){
  const {id} = req.params;
  const {name, image} = req.body;
  db.query(
    `UPDATE cats SET name="${name}", image="${image}" WHERE id=${id}`,
    function (error, results, fields){
      if (error) throw error;
    }
  );
  db.query(
    `SELECT * FROM cats WHERE id=${id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});
 
const PORT = 3001;
app.listen(PORT, () => console.log("Server started on port 3001"));