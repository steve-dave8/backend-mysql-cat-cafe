SHOW DATABASES; 

CREATE DATABASE db_name;

USE db_name;

SHOW TABLES;

SELECT * FROM table_name;

ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';

INSERT INTO cats (name, image) VALUES
